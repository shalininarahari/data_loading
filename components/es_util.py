from elasticsearch import Elasticsearch


class ElasticSearchManager():
    def __init__(self):
        self.hosts = ['10.32.102.180']
        self.port = 9200
        self.connection = None
    def __enter__(self):
        self.connection = Elasticsearch(self.hosts, port=self.port)
        return self
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.transport.close()

