from datetime import datetime, timedelta
import random
from components.es_util import ElasticSearchManager
import uuid
import parser
import pytz
from components import config
count=config.count
start_date=config.start_date
end_date=config.end_date
def claimtypes_select():
    claimTypes = ["ITS HOST Institutional:INSURANCE_PROVIDER_1","ITS HOST Professional:INSURANCE_PROVIDER_2","Commercial Professional:INSURANCE_PROVIDER_3","Commercial Institutional:INSURANCE_PROVIDER_4","Cross Over Institutional:INSURANCE_PROVIDER_5","Paper Professional:INSURANCE_PROVIDER_6",
                 "Cross Over Professional:INSURANCE_PROVIDER_7", "Paper Institutional:INSURANCE_PROVIDER_8","Paper Unicare Institutional:INSURANCE_PROVIDER_9","Paper Unicare Professional:INSURANCE_PROVIDER_10","ITS HOST Paper Professional:INSURANCE_PROVIDER_11","ITS HOST Paper Institutional:INSURANCE_PROVIDER_12"]
    claimType = random.choice(claimTypes)
    return claimType

def standard_Date_format(timestamp):
    try:
        timezone = pytz.timezone('UTC')
        standard_time= timezone.localize(timestamp).isoformat()
        return standard_time
    except Exception as e:
        print("exception occured when timestamp into standard timestamp due to :", e)
        standard_time = timestamp
        return standard_time

def insert_record_es(record):
 with ElasticSearchManager() as es:
    es.connection.index(index=config.index_name,
                        doc_type='post',
                        id=(uuid.uuid1()).hex,
                        body=record,
                        request_timeout=30,
                        )
    """set_mapping = {
        "properties": {
            "processName": {
                "type": "text",
                "fielddata": True
            }
        }
    }
    set_read = {
        "index": {
            "blocks": {
                "read_only_allow_delete": "false"
            }
        }
    }
   
    es.connection.update(index=config.index_name, body=set_read)
    es.connection.update(index=config.index_name, body=set_mapping)"""

def p8(record):
    record['processName'] = 'WGS_EDIT_ADJUDICATION_FINALIZATION'
    record['code'] = 201
    insert_record_es(record)
    code = [400, 210]
    record['code'] = random.choice(code)
    insert_record_es(record)

def p7(record):
    record['processName'] = 'WGS_LOAD_POSTBACK'
    record['code'] = 201
    insert_record_es(record)
    code = [400, 210]
    record['code'] = random.choice(code)
    insert_record_es(record)
    if record['code'] == 210:
        p8(record)

def p6(record):
    record['processName'] = 'WGS_PREMAPPER_POSTBACK'
    record['code'] = 201
    insert_record_es(record)
    code = [400, 210]
    record['code'] = random.choice(code)
    insert_record_es(record)
    if record['code'] == 210:
        p7(record)

def p5(record):
    record['processName'] = 'FILE_GENERATION'
    record['code'] = 201
    insert_record_es(record)
    code = [400, 210]
    record['code'] = random.choice(code)
    insert_record_es(record)
    if record['code'] == 210:
        p6(record)

def p4(record):
    record['processName'] = 'OBJECT_GENERATION'
    record['code'] = 201
    insert_record_es(record)
    code = [400, 210]
    record['code'] = random.choice(code)
    insert_record_es(record)
    if record['code'] == 210:
        p5(record)

def p3(record):
        record['processName'] = 'ORCHESTRATION'
        record['code'] = 201
        insert_record_es(record)
        code = [400, 210]
        record['code'] = random.choice(code)
        insert_record_es(record)
        if record['code'] == 210:
            p4(record)
def p2(record):
    record['processName'] = 'CLAIM_CREATION'
    record['code'] = 201
    insert_record_es(record)
    code = [400, 210, 201]
    record['code'] = random.choice(code)
    insert_record_es(record)
    if record['code'] == 210:
        p3(record)

def p1(count,start_date,end_date):
  with ElasticSearchManager() as es:
    start_date = datetime.strptime(start_date, '%Y-%m-%d %H:%M:%S.%f')
    end_date = datetime.strptime(end_date, '%Y-%m-%d %H:%M:%S.%f')
    daterange =(end_date - start_date).days
    record= {
        "timeStamp": "2019-12-01T06:05:29.567000+05:30",
        "workStartRecordStatus": "Pending-Kernel",
        "workEndRecordStatus": "Resolved-Paid",
        "outWGS": 3,
        "processName": "EDI_837_INGESTION",
        "jobId": "N837File6031",
        "claimsWorkId": "CLM_18100810",
        "agentNumber": 30,
        "AGENTNODEID": "X12BatchClaimAdjudication 43048e13cfb9dbec611c5d9ef98ef4e4",
        "fileListenerNodeId": "41dbe830230311eab358f8cab83002f3",
        "EXCEPTIONMESSAGE": "NA",
        "CLAIM_TYPE": "Paper Unicare Institutional",
        "recordId": "CDE18000811",
        "code": "201",
        "providerName": "INSURANCE_PROVIDER_9"
    }
    for x in range(daterange+1):
        claim_id=190000000
        claim_work_id=180000000
        secondscount=0
        REC_PROCESS_TIMESTAMP=start_date
        number_of_cliams_per_day=count
        for claims_count in range(number_of_cliams_per_day):
            claim_id=claim_id+1
            claim_work_id=claim_work_id+1
            secondscount = secondscount + 1
            if (secondscount == 100):
                REC_PROCESS_TIMESTAMP = start_date + timedelta(seconds=1)
            record['timeStamp'] = standard_Date_format(REC_PROCESS_TIMESTAMP)
            record['processName']="EDI_837_INGESTION"
            record['claimsId'] = 'cde'+str(claim_id)
            record['EXCEPTIONMESSAGE'] = ""
            CLAIM_TYPE = claimtypes_select()
            claimType = CLAIM_TYPE.split(":")
            record['CLAIM_TYPE'] = claimType[0]
            record['providerName'] = claimType[1]
            record['recordId'] = 'CLM_'+str(claim_work_id)
            code = [210, 400, 201]
            record['code']= random.choice(code)
            insert_record_es(record)
            record['code'] =random.choice(code)
            insert_record_es(record)
            if record['code'] == 210:
                p2(record)
        start_date = start_date+ timedelta(days=1)





p1(count,start_date,end_date)